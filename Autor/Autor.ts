export class Autor {
  private _nome: string;
  private _Data?: Date;
  birthDate: any;

  constructor(nome: string, Data?: Date) {
    this._nome = nome;
    this._Data = Data;
  }

  public get nome() {
    return this._nome;
  }

  public set nome(newNome: string) {
    this._nome = newNome;
  }

  public get Data() {
    return this._Data;
  }

  public set Data(newData: Date) {
    this._Data = newData;
  }
}