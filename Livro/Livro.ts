import { Capitulo } from "../Capitulo/Capitulo";
import { Autor } from "../Autor/Autor";

export class Livro {
  private _titulo: string;
  private _isbn: string;
  private _capituloArr: Array<Capitulo> = [];
  private _autorArr: Array<Autor> = [];

  constructor(
    titulo: string,
    isbn: string,
    capitulosArr: Array<Capitulo>,
    autoresArr: Array<Autor>
  ) {
    this._titulo = titulo;
    this._isbn = isbn;
    this._capituloArr = capitulosArr;
    this._autorArr = autoresArr;
  }

  public get capitulosArr() {
    return this._capituloArr;
  }
  public set capitulosArr(novoCapitulosArr: Array<Capitulo>) {
    this._capituloArr = novoCapitulosArr;
  }

  public get autoresArr() {
    return this._autorArr;
  }
  public set autoresArr(novoAutoresArr: Array<Autor>) {
    this._autorArr = novoAutoresArr;
  }

  public get isbn() {
    return this._isbn;
  }
  public set isbn(novoIsbn: string) {
    this._isbn = novoIsbn;
  }

  public get titulo() {
    return this._titulo;
  }
  public set titulo(novotitulo: string) {
    this._titulo = novotitulo;
  }

  public addCapitulo(titulo: string, text: string) {}

  public removeCapitulo(removeCapitulo: Capitulo) {}

  public addAutor(autor: Autor) {}

  public removeAutor(remAutor: Autor) {}
}