import { Livro } from "./Livro/Livro";
import { Autor } from "./Autor/Autor";
import { Capitulo } from "./Capitulo/Capitulo";
import { erroCod, csl, formSpace, prompt, limpar } from "./Dependencias/Dependencias";

limpar();
const livrosArray: Array<Livro> = [];

class livroStore {
  public menu() {
    let option: number = 0;
    while (option !== 9) {
      csl(`|------------------------------------------|`);
      csl(`|             Livraria Konoha              |`);
      csl(`|------------------------------------------|`);
      csl(`| 1. Cadastrar novo livro                  |`);
      csl(`| 2. Remover livro do acervo               |`);
      csl(`| 3. Listar acervo                         |`);
      csl(`| 4. Resetar livraria                      |`);
      csl(`| 5. Modificar Livro do Acervo             |`);
      csl(`| 6. Listar capítulos de livro             |`);
      csl(`| 9. Sair                                  |`);
      csl(`|------------------------------------------|`);
      option = Number(prompt("Escolha uma opção: "));

      switch (option) {
        case 1:
          this.criarLivro();
          break;
        case 2:
          this.removerLivro();
          break;
        case 3:
          this.mostrarLivros();
          break;
        case 4:
          this.limparLoja();
          break;
        case 5:
          this.mudarLivro();
          break;
        case 6:
          this.listarCapitulos();
          break;
        case 9:
          break;
        default:
          erroCod();
      }
    }
  }

  private criarLivro() {
    limpar();

    const capitulosArray: Array<Capitulo> = [];
    const autoresArray: Array<Autor> = [];

    csl(`|------------------------------------------|`);
    csl(`|              Incluir Livro               |`);
    csl(`|------------------------------------------|`);

    const novoLivroTitulo: string = prompt("Digite o nome do livro: ");
    const novoLivroIsbn: string = prompt(
      "Digite o International Standard Book Number (ISBN): "
    );

    csl(`|------------------------------------------|`);

    const autoresNumber = prompt("Digite o número de autores a cadastrar: ");
    if (autoresNumber == 0 || autoresNumber == isNaN) {
      erroCod();
      return;
    } else {
      let contAutor = 1;

      while (contAutor <= autoresNumber) {
        csl(`Cadastro de Autor ${contAutor}/${autoresNumber}`);
        const novoNome: string = prompt("Digite o nome do autor: ");
        const novaData: Date = prompt(
          "Digite a data de nascimento do autor: "
        );
        const novoAutor = new Autor(novoNome, novaData);
        autoresArray.push(novoAutor);
        contAutor++;
      }
    }

    csl(`|------------------------------------------|`);

    const chaptersNumber = prompt("Digite o número de capítulos a cadastrar: ");
    if (chaptersNumber == 0 || chaptersNumber == isNaN) {
      erroCod();
      return;
    } else {
      let contChapters = 1;

      while (contChapters <= chaptersNumber) {
        csl(`Cadastro de Autor ${contChapters}/${chaptersNumber}`);
        const novoTituloCapitulo: string = prompt("Digite o título do capítulo: ");
        const novoTextoCapitulo: string = prompt("Digite o texto do capítulo: ");
        const novoCapitulo = new Capitulo(novoTituloCapitulo, novoTextoCapitulo);
        capitulosArray.push(novoCapitulo);
        contChapters++;
      }
    }

    const novoLivro = new Livro(
      novoLivroTitulo,
      novoLivroIsbn,
      capitulosArray,
      autoresArray
    );

    livrosArray.push(novoLivro);
    limpar();
    csl("Livro cadastrado com sucesso!");
  }

  private removerLivro() {
    limpar();
    csl(`|------------------------------------------|`);
    csl(`|              Livros Cadastrados          |`);
    csl(`|------------------------------------------|`);
    csl("");
    this.listarLivroIndex();
    const selecionarLivro = prompt("\nSelecione um livro para remover: ");
    if (selecionarLivro < 0 || selecionarLivro > livrosArray.length) {
      csl(`Livro na posição ${selecionarLivro} inválida`);
    } else {
      limpar();
      csl(`Livro ${livrosArray[selecionarLivro].titulo} removido com sucesso!`);
      selecionarLivro.splice(selecionarLivro, 1);
    }
  }

  private mostrarLivros() {
    const livrosArrayLen: number = livrosArray.length;

    limpar();
    csl(`|------------------------------------------|`);
    csl(`|              Listar Livros               |`);
    csl(`|------------------------------------------|`);
    csl("");
    csl("");

    if (livrosArrayLen == 0) {
      csl("Não há livros cadastrados");
      csl('')
      return;
    }
    
    let i: number;
    for (i = 1; i <= livrosArrayLen; i++) {
      csl(`Livro ${i} no index ${i - 1} do array \n`);
      csl(`Título: ${livrosArray[i - 1].titulo}\n`);
      csl(`ISBN: ${livrosArray[i - 1].isbn}\n`);

      csl("Autores:");
      let x: any;
      for (x in livrosArray[i - 1].autoresArr) {
        const partNome = livrosArray[i - 1].autoresArr[x].nome;
        const partData = livrosArray[i - 1].autoresArr[x].Data;
        csl(`${partNome}${formSpace(partNome)}${partData}`);
      }

      csl("\nCapítulos:");
      let y: any;
      for (y in livrosArray[i - 1].capitulosArr) {
        const partTitulo = livrosArray[i - 1].capitulosArr[y].titulo;
        const partTexto = livrosArray[i - 1].capitulosArr[y].text;
        csl(`Título: ${partTitulo}${formSpace(partTitulo)}${partTexto}`);
      }
      csl(`|------------------------------------------|`);
      csl("");
    }
  }

  private limparLoja() {
    limpar();
    const livrosArrayLen: number = livrosArray.length;
    let i: number;
    for (i = 0; i < livrosArrayLen; i++) {
      livrosArray.shift();
    }
    livrosArray.length == 0
      ? csl("Livaria resetada com sucesso.")
      : erroCod();
  }

  private mudarLivro() {
    this.listarLivroIndex();
    const listarCapituloIndex = Number(
      prompt(`Digite o ID do livro para alterar: \n`)
    );

    csl(`1. Inserir novo Autor`);
    csl(`2. Inserir novo Capítulo`);
    const mudar = Number(prompt("Escolha uma opção: "));

    if (mudar == 1) {
      this.addAutor(listarCapituloIndex);
    } else if (mudar == 2) {
      this.addAutor(listarCapituloIndex);
    } else {
      erroCod();
    }
  }

  private addAutor(bookIndex: number) {
    limpar();
    const novoNome: string = prompt("Digite o nome do autor: ");
    const novaData: Date = prompt(
      "Digite a data de nascimento do autor: "
    );
    const novoAutor = new Autor(novoNome, novaData);
    livrosArray[bookIndex].autoresArr.push(novoAutor);
  }

  private addCapitulo(bookIndex: number) {
    limpar();
    const novoTituloCapitulo: string = prompt("Digite o título do capítulo: ");
    const novoTextoCapitulo: string = prompt("Digite o texto do capítulo: ");
    const novoCapitulo = new Capitulo (novoTituloCapitulo, novoTextoCapitulo);
    livrosArray[bookIndex].capitulosArr.push(novoCapitulo);
  }

  private listarCapitulos() {
    this.listarTituloIndex();
    const listChapterIndex = Number(
      prompt(`Deseja listar o capítulo de qual livro? `)
    );
    let i;
    for (i = 0; i < livrosArray[listChapterIndex].capitulosArr.length; i++) {
      csl(`${livrosArray[listChapterIndex].capitulosArr[i].titulo}`);
    }
  }
  listarTituloIndex() {
    throw new Error("Não implementado.");
  }

  private listarLivroIndex() {
    limpar();
    let x;
    for (x in livrosArray) {
      csl(`${x}. ${livrosArray[x].titulo}`);
    }
    csl("");
  }
}

const novolivro = new livroStore();

novolivro.menu();